import datetime
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import *

response = {}


def index(request):
    if (not Message.objects.all()):
        Message.objects.create(sender="Arga Ghulam Ahmad", receiver="You", text="Hello, World!", isFeatured=True)

    response['featured'] = Message.objects.filter(isFeatured=True)[0]
    response['messages'] = list(reversed(Message.objects.all()))
    return render(request, 'base.html', response)


@csrf_exempt
def add_message(request):
    if request.method == 'POST':
        sender = request.POST['sender']
        receiver = request.POST['receiver']
        message = request.POST['message']
        obj_message = Message(sender=sender, receiver=receiver, text=message)
        obj_message.save()

        str_datetime = (datetime.datetime.now().strftime('%b. %d, %Y, %-I:%M %p'))
        if (str_datetime[len(str_datetime) - 2:] == 'AM'):
            str_datetime = str_datetime[:len(str_datetime) - 2] + 'a.m.'
        elif (str_datetime[len(str_datetime) - 2:] == 'PM'):
            str_datetime = str_datetime[:len(str_datetime) - 2] + 'p.m.'
        return JsonResponse({'status': 'success', 'date_time': str_datetime})
    else:
        return JsonResponse({'status': 'failed'})
