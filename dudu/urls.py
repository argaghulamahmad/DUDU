from django.conf.urls import url
from .views import index, add_message

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add-message/$', add_message, name='add-message'),
]