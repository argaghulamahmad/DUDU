from django.db import models


class Message(models.Model):
    sender = models.CharField(max_length=27)
    receiver = models.CharField(max_length=27)
    text = models.TextField(max_length=140)
    isFeatured = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.sender + ' to ' + self.receiver + ' - Message ' + str(self.pk)