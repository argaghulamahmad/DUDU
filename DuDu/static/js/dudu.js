$(document).ready(function () {
    $("#submit-button").click(function () {
        var receiver = $("#receiver").val();
        var sender = $("#sender").val();
        var message = $("#message").val();
        var is_checked = document.getElementById('agree-checkbox').checked;

        console.log(receiver + ' to ' + sender);
        console.log('Message: ' + message);
        console.log(is_checked);

        if (receiver !== '' && sender !== '') {
            if (message !== '') {
                if (is_checked) {
                    addMessage(sender, receiver, message)
                } else {
                    alert("You must agree terms and condition!")
                }
            } else {
                alert("Message field must be filled!")
            }
        } else {
            alert("You must fill 'from' and 'to' field!")
        }
    });

    var addMessage = function (sender, receiver, message) {
        $.ajax({
            method: "POST",
            url: "{% url "dudu: add - message" %}",
            data: {sender: sender, receiver: receiver, message: message},
            success: function (data) {
                if (data.status === 'success') {
                    console.log(data.status);
                    var message_area = $("#messages-area");
                    var messages = message_area.html();
                    var date_time = data.date_time;
                    message_area.html(
                        "<div class=\"card border-gap\">" +
                        "<div class=\"card-body\">" +
                        "<h4 class=\"card-title\">" + sender + " to " + receiver + "</h4>" +
                        "<p class=\"card-text\">" + message + "</p>" +
                        "</div>" +
                        "<br>" +
                        "<footer>" + date_time + "</footer>" +
                        "</div>" + messages
                    )
                } else {
                    console.log(data.status);
                }
            },
            error: function (error) {
                alert(error.responseText)
            }
        });
    };
});