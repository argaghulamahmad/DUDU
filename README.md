#Aplikasi Web 'Dari Untuk'
[![Dari Untuk](https://bitbucket.org/argaghulamahmad/image-hosting/raw/b5d13302a22f761ed2ce15dd4b85965dfb6dcafa/dudu/DariUntuk.png "Dari Untuk")](http://dariuntuk.herokuapp.com "Dari Untuk")

Aplikasi web ini dikembangkan menggunakan framework Django, Python, dbSqlite, HTML, CSS, JS, AJAX, dan jQuery. Aplikasi ini telah di deploy ke Heroku yang dapat diakses melalui [http://dariuntuk.herokuapp.com/](http://dariuntuk.herokuapp.com "Dari Untuk").

Created with love by Arga Ghulam Ahmad © 2018.